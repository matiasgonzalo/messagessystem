<?php

use App\User;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\Hash;

class FakeUsersSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $superAdministrator = new User;
        $superAdministrator->name = 'mati';
        $superAdministrator->email = 'mati@gmail.com';
        $superAdministrator->password = Hash::make('123123');
        $superAdministrator->save();
        $superAdministrator->addMedia(public_path('img/a4.jpg'))->preservingOriginal()->toMediaCollection('avatar');

        $superAdministrator->assignRole("superAdministrador");

        $administrador = new User;
        $administrador->name = 'pedro';
        $administrador->email = 'pedro@gmail.com';
        $administrador->password = Hash::make('123123');
        $administrador->save();
        $administrador->addMedia(public_path('img/a4.jpg'))->preservingOriginal()->toMediaCollection('avatar');

        $administrador->assignRole("administrador");

        $partner = new User;
        $partner->name = 'pedropartner';
        $partner->email = 'pedropartner@gmail.com';
        $partner->password = Hash::make('123123');
        $partner->save();
        $partner->addMedia(public_path('img/a4.jpg'))->preservingOriginal()->toMediaCollection('avatar');

        $partner->assignRole("partner");

        $administrador->partners()->save($partner);

        $vendedor = new User;
        $vendedor->name = 'david';
        $vendedor->email = 'david@gmail.com';
        $vendedor->password = Hash::make('123123');
        $vendedor->save();
        $vendedor->addMedia(public_path('img/a4.jpg'))->preservingOriginal()->toMediaCollection('avatar');

        $vendedor->assignRole("vendedor");

        $partner->vendedores()->save($vendedor);

        factory(User::class, 10)->create()->each(function ($user) {
            $partner = User::role('partner')->first();
            $user->assignRole("vendedor");
            $partner->vendedores()->save($user);
            $user->addMedia(public_path('img/a4.jpg'))->preservingOriginal()->toMediaCollection('avatar');
        });
    }
}
