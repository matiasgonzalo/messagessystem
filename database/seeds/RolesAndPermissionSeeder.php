<?php

use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;

class RolesAndPermissionSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        app()['cache']->forget('spatie.permissions.cache');

        //Create

        $superAdministrator = Role::create(['name' => 'superAdministrador']);

        $administrator = Role::create(['name' => 'administrador']);

        $employed = Role::create(['name' => 'employed']);

        $partner = Role::create(['name' => 'partner']);

        $vendedor = Role::create(['name' => 'vendedor']);

        $comprador = Role::create(['name' => 'comprador']);
    }
}
