<?php

use App\User;
use App\Message;
use Carbon\Carbon;
use App\Conversation;
use Illuminate\Database\Seeder;

class FakeMessagesSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $emisor = User::role('superAdministrador')->first();
        $receptor = User::role('administrador')->first();

        $conversation = Conversation::create([
            'id_latest_receptor' => $receptor->id,
            'asunto' => 'Primera prueba',
            'latest_message' => 'Hola, me llamo matias, me alegro que estés bien',
            'latest_message_date' => Carbon::now()
        ]);

        $message = Message::create([
            'message' => 'Hola, me llamo matias, me alegro que estés bien',
        ]);

        $emisor->messages_emisor()->save($message);

        $receptor->messages_receptor()->save($message);

        $conversation->messages()->save($message);

        $emisor->conversations_emisor()->save($conversation);

        $receptor->conversations_receptor()->save($conversation);
    }
}
