<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateConversationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('conversations', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('emisor_id')->unsigned()->nullable();
            $table->bigInteger('receptor_id')->unsigned()->nullable();
            $table->string('asunto');
            $table->integer('id_latest_receptor')->unsigned();
            $table->text('latest_message');
            $table->timestamp('latest_message_date');
            $table->boolean('read_by_latest_receptor')->default(false);
            $table->boolean('deleted_first_level_emisor')->default(false);
            $table->boolean('deleted_first_level_receptor')->default(false);
            $table->boolean('deleted_second_level_emisor')->default(false);
            $table->boolean('deleted_second_level_receptor')->default(false);
            $table->timestamps();

            $table->foreign('emisor_id')->references('id')->on('users')->onDelete('set null');
            $table->foreign('receptor_id')->references('id')->on('users')->onDelete('set null');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('conversations');
    }
}
