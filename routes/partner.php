<?php

Route::get('partner/vendedores', [
    'uses' => 'VendedorController@index',
    'as' => 'vendedores.index'
]);

Route::get('/vendedores/{id}', [
    'uses' => 'VendedorController@show',
    'as' => 'vendedores.show'
]);

Route::delete('/vendedores/{id}', [
    'uses' => 'VendedorController@destroy',
    'as' => 'vendedores.destroy'
]);

