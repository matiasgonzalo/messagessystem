<?php

Route::put('mails/{conversation}/update', [
    'uses' => 'MessageController@update',
    'as' => 'conversations.update'
]);

Route::get('mail/create', [
    'uses' => 'MessageController@create',
    'as' => 'mail.create'
]);

Route::get('/conversations', [
    'uses' => 'MessageController@index',
    'as' => 'conversations.index'
]);

Route::get('/conversation/{conversation}', [
    'uses' => 'MessageController@show',
    'as' => 'conversations.show'
]);

Route::get('/conversation-eliminado/{conversation}', [
    'uses' => 'MessageController@showEliminado',
    'as' => 'conversations.show.eliminado'
]);

Route::post('/mail', [
    'uses' => 'MessageController@store',
    'as' => 'mail.store'
]);

Route::post('/mail/store_mail_privado', [
    'uses' => 'MessageController@store_mail_privado',
    'as' => 'mail.store_mail_privado'
]);

Route::post('/delete_first_level', [
    'uses' => 'MessageController@delete_first_level',
    'as' =>  'conversations.delete_first_level'
]);

Route::post('/delete_second_level', [
    'uses' => 'MessageController@delete_second_level',
    'as' =>  'conversations.delete_second_level'
]);

Route::post('/delete_first_level_group', [
    'uses' => 'MessageController@delete_first_level_group',
    'as' =>  'conversations.delete_first_level_group'
]);

Route::post('/delete_second_level_group', [
    'uses' => 'MessageController@delete_second_level_group',
    'as' =>  'conversations.delete_second_level_group'
]);

Route::get('/conversations-deleted', [
    'uses' => 'MessageController@indexEliminados',
    'as' =>  'conversations.index_eliminados'
]);

Route::post('/mail-eliminar-permanentemente', [
    'uses' => 'MessageController@eliminar_permanentemente',
    'as' =>  'mail.eliminar_permanentemente'
]);

Route::post('/restaurar-conversations', [
    'uses' => 'MessageController@restore_conversations',
    'as' =>  'conversations.conversations.restore'
]);

Route::get('/responder-mensaje/{conversation}', [
    'uses' => 'MessageController@responder',
    'as' => 'conversations.response'
]);

Route::post('/buscar-mensaje-recibido', [
    'uses' => 'MessageController@buscarMensajeRecibido',
    'as' => 'mail.buscar_recibido'
]);

Route::post('/buscar-mensaje-eliminado', [
    'uses' => 'MailController@buscarMensajeEliminado',
    'as' => 'mail.buscar_eliminado'
]);
