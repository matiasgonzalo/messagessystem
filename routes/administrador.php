<?php

Route::get('/partners/{id}', [
    'uses' => 'PartnerController@show',
    'as' => 'partners.show'
]);

Route::delete('/partners/{id}', [
    'uses' => 'PartnerController@destroy',
    'as' => 'partners.destroy'
]);

Route::get('/partners', [
    'uses' => 'PartnerController@index',
    'as' => 'partners.index'
]);
