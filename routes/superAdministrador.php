<?php

Route::get('/administradores', [
    'uses' => 'AdministradorController@index',
    'as' => 'administradores.index'
]);

Route::get('/administradores/{id}', [
    'uses' => 'AdministradorController@show',
    'as' => 'administradores.show'
]);

Route::delete('/administradores/{id}', [
    'uses' => 'AdministradorController@destroy',
    'as' => 'administradores.destroy'
]);
