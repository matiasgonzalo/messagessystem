<link rel="shortcut icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">
<link rel="icon" href="{{ asset('favicon.ico') }}" type="image/x-icon">


<!-- CSRF Token -->
<meta name="csrf-token" content="{{ csrf_token() }}">

<link href="{{ asset('template/css/bootstrap.min.css') }}" rel="stylesheet">

<link href="{{ asset('template/font-awesome/css/font-awesome.css') }}" rel="stylesheet">

<link href="{{ asset('template/css/plugins/iCheck/custom.css') }}" rel="stylesheet">

<!-- Sweet Alert -->
<link href="{{ asset('template/css/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet">

<!-- Summernote -->
<link href="{{ asset('template/css/plugins/summernote/summernote.css') }}" rel="stylesheet">
<link href="{{ asset('template/css/plugins/summernote/summernote-bs3.css') }}" rel="stylesheet">

<link href="{{ asset('template/css/animate.css') }}" rel="stylesheet">
<link href="{{ asset('template/css/style.css') }}" rel="stylesheet">

<!-- Latest compiled and minified CSS -->
<link rel="stylesheet" href="{{ asset('bootstrap-select/dist/css/bootstrap-select.css') }}">
