<!-- Mainly scripts -->
<script>
    window.Laravel = {!! json_encode([
        'user' => auth()->check() ? auth()->user()->id : null,
    ]) !!};

</script>

<script src="{{ asset('template/js/jquery-3.1.1.min.js') }}"></script>

<script src="{{ asset('template/js/bootstrap.js') }}"></script>

<script src="{{ asset('template/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
<script src="{{ asset('template/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>

<!-- Custom and plugin javascript -->
<script src="{{ asset('template/js/inspinia.js') }}"></script>
<script src="{{ asset('template/js/plugins/pace/pace.min.js') }}"></script>

<!-- iCheck -->
<script src="{{ asset('template/js/plugins/iCheck/icheck.min.js') }}"></script>
<script>
    $(document).ready(function () {
        $('.i-checks').iCheck({
            checkboxClass: 'icheckbox_square-green',
            radioClass: 'iradio_square-green',
        });
    });
</script>

<!-- SUMMERNOTE -->
<script src="{{ asset('template/js/plugins/summernote/summernote.min.js') }}"></script>

<script>
    $(document).ready(function(){

        $('.summernote').summernote({
            placeholder: 'Escriba su mensaje aquí...',
            height: 200
        });

    });
</script>

<!-- Sweet alert -->
<script src="{{ asset('template/js/plugins/sweetalert/sweetalert.min.js') }}"></script>

<script>
$(document).ready(function () {
    $('.confirm-pay-now').click(function () {
        var formName = '#pay-form-now-'+$(this).data('id');
        swal({
            title: "Está seguro?",
            text: "Una vez confirmado no podrá volver atrás!",
            type: "warning",
            showCancelButton: true,
            confirmButtonColor: "#DD6B55",
            confirmButtonText: "Confirmar",
            closeOnConfirm: false,
            closeOnCancel: false
        }, function (isConfirm) {
            if (isConfirm) {
                $(formName).submit();
            }else{
                swal("Cancelado", "La operación fue cancelada", "error");
            }
        });
    });
});
</script>

@if(Session::has('message'))
    <script>
        $(document).ready(function () {
            let alertType = "{{ Session::get('alert-type', 'info') }}";

            let message = '{{ Session::get('message') }}';

            let title = 'Información';

            switch (alertType) {
                case 'info':
                    title = 'Information';
                    break;
                case 'warning':
                    title = 'Warning';
                    break;
                case 'success':
                    title = 'Ok';
                    break;
                case 'error':
                    title = 'Oops';
                    break;
            }
            swal(title, message, alertType);
        });
    </script>
@endif

<!-- Latest compiled and minified JavaScript -->
<script src="{{ asset('bootstrap-select/dist/js/bootstrap-select.min.js') }}"></script>
