<!DOCTYPE html>
<html>

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <title>MESSAGES | SYS</title>

    @include('layouts.links')
</head>

<body class="pace-done skin-1">
    <div id="app">
        <div id="wrapper">
            <nav class="navbar-default navbar-static-side" role="navigation">
                <div class="sidebar-collapse">
                    <ul class="nav metismenu" id="side-menu">
                        <li class="nav-header">
                            <div class="dropdown profile-element"> <span>
                                    <img alt="image" width="50" class="img-circle" src="{{ asset(Auth::user()->getFirstMediaUrl('avatar', 'thumb')) }}" />
                                    </span>
                                <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                                    <span class="clear"> <span class="block m-t-xs"> <strong class="font-bold">{{ Auth::user()->name }}</strong>
                                    </span> <span class="text-muted text-xs block">{{ Auth::user()->roles->first()->name }} <b class="caret"></b></span> </span> </a>
                                <ul class="dropdown-menu animated fadeInRight m-t-xs">
                                    <li><a href="{{ route('conversations.index') }}">Mensajes recibidos</a></li>
                                    <li class="divider"></li>
                                    <li><a href="{{ route('logout') }}" onclick="event.preventDefault();
                                        document.getElementById('logout-form').submit();">
                                            <i class="fa fa-sign-out"></i> Salir
                                        </a>
                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </li>
                                </ul>
                            </div>
                            <div class="logo-element">
                                MOL
                            </div>
                        </li>
                        <li>
                            <a href="{{ route('home') }}"><i class="fa fa-th-large"></i> <span class="nav-label">Home</span></a>
                        </li>
                        <li>
                            <a href="{{ route('vendedores.index') }}"><i class="fa fa-handshake-o"></i> <span class="nav-label">Vendedores</span></a>
                        </li>
                        <li>
                                <a href="mailbox.html"><i class="fa fa-envelope"></i> <span class="nav-label">Mensajes </span><span class="label label-warning pull-right">{{ Auth::user()->messages_count_unread() }}</span></a>
                            <ul class="nav nav-second-level collapse">
                                <li><a href="{{ route('conversations.index') }}">Mensajes Recibidos</a></li>
                                <li><a href="{{ route('mail.create') }}">Crear Mensaje</a></li>
                            </ul>
                        </li>
                    </ul>
                </div>
            </nav>

            <div id="page-wrapper" class="gray-bg">
                <div class="row border-bottom white-bg">
                    @include('layouts.nav_superior')
                </div>

                @yield('content')

            </div>
        </div>
    </div>

    @include('layouts.scripts')

</body>

</html>
