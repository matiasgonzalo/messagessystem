@extends('layouts.app' . Auth::user()->getRole())

@section('content')
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-3">
                @include('messages.menuNavegacion')
            </div>
            <div class="col-lg-9 animated fadeInRight">
                <div class="mail-box-header">
                    <h2>
                        Ver mensaje
                    </h2>
                    <div class="mail-tools tooltip-demo m-t-md">
                        <h3>
                            <span class="font-normal">Asunto: </span>{{ $conversation->asunto }}
                        </h3>
                        <h5>
                            <span class="font-normal">De: </span>{{ $conversation->messages()->latest()->first()->emisor->email }}
                        </h5>
                    </div>
                </div>
                <div class="mail-box">
                    @foreach ($conversation->messages as $message)
                    <div class="mail-body">
                            <div>
                                <span class="pull-right font-normal">{{ $message->created_at }}</span>
                            </div>
                            {!! $message->message !!}
                        </div>
                    @endforeach
                    <div class="mail-body text-right tooltip-demo">
                        <button type="submit" form="delete_first_level" title="Eliminar Permanentemente" data-placement="top" data-toggle="tooltip" class="btn btn-sm btn-danger"><i class="fa fa-trash-o"></i> Eliminar</button>
                    </div>
                    <form id="delete_first_level" method="POST" action="{{ route('conversations.delete_second_level') }}">
                        @csrf
                        <input type="hidden" name="conversation" value="{{ $conversation->id }}">
                    </form>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
@endsection
