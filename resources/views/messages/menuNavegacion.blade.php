<div class="ibox float-e-margins">
    <div style="border:none;" class="ibox-content mailbox-content">
        <div class="file-manager">
            <a class="btn btn-block btn-primary compose-mail" href="{{ route('mail.create') }}">Crear Mensaje</a>
            <div class="space-25"></div>
            <h5>Carpetas</h5>
            <ul class="folder-list m-b-md" style="padding: 0">
            <li><a href="{{ route('conversations.index') }}"> <i class="fa fa-inbox "></i> Mensajes recibidos <span class="label label-warning pull-right">{{ Auth::user()->messages_count_unread() }}</span> </a></li>
                <li><a href="{{ route('mail.create') }}"> <i class="fa fa-envelope-o"></i> Enviar mensaje</a></li>
                <li><a href="{{ route('conversations.index_eliminados') }}"> <i class="fa fa-trash-o"></i> Mensajes eliminados</a></li>
            </ul>
            <div class="clearfix"></div>
        </div>
    </div>
</div>
