@extends('layouts.app' . Auth::user()->getRole())

@section('content')
<!-- Contenido -->
<div class="wrapper wrapper-content">
    <div class="row">
        <div class="col-lg-3">
            @include('messages.menuNavegacion')
        </div>
        <div class="col-lg-9 animated fadeInRight">
            <div class="mail-box-header">
                <form method="POST" action="{{ route('mail.buscar_recibido') }}" class="pull-right mail-search">
                    @csrf
                    <div class="input-group">

                        <input type="text" class="form-control input-sm" name="mensaje" placeholder="Buscar Mensaje">

                        <div class="input-group-btn">
                            <button type="submit" class="btn btn-sm btn-primary">
                                Buscar
                            </button>
                        </div>
                    </div>
                </form>
                <h2>
                    Conversaciones ({{ $usuario->conversations_count() }})
                </h2>
                <div class="mail-tools tooltip-demo m-t-md">
                    <div class="btn-group pull-right">
                        {!! $conversations->links() !!}
                    </div>
                    <a href="{{ route('conversations.index') }}" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="left" title="Refrescar lista de mensajes de entrada"><i class="fa fa-refresh"></i> Refrescar</a>
                    <form style="display:inline;" method="POST" id="marcar_borrado" action="{{ route('conversations.delete_first_level_group') }}">
                        @csrf
                        <button type="submit" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Mover a la carpeta de mensajes eliminados"><i class="fa fa-trash-o"></i> </button>
                    </form>
                </div>
            </div>
            <div class="mail-box">
                <table class="table table-hover table-mail">
                    <tbody  id="messeges_index">
                        @foreach($conversations as $conversation)
                            <tr class="{{ $conversation->read_latest_message($usuario) }}">
                                <td class="check-mail">
                                    <input form="marcar_borrado"  type="checkbox" value="{{ $conversation->id }}"  name="conversations[]" class="i-checks">
                                </td>
                                <td class="mail-ontact"><a href="{{ route('conversations.show', $conversation) }}">{{ $conversation->latest_message_for_user($usuario)->emisor->name }}</a> </td>
                                <td ><span style="float:left !important;" class="label label-{{ $conversation->latest_message_for_user($usuario)->emisor->getClassForRole() }} pull-right">{{ $conversation->latest_message_for_user($usuario)->emisor->getRole() }}</span></td>
                                <td class="mail-subject"><a href="{{ route('conversations.show', $conversation) }}">{{ $conversation->asunto }}</a></td>
                                <td class="text-right mail-date">{{ $conversation->fecha($conversation->latest_message_for_user($usuario)->created_at) }}  {{ $conversation->hora($conversation->latest_message_for_user($usuario)->created_at) }}</td>
                            </tr>
                        @endforeach
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

@endsection
