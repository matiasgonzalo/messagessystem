@extends('layouts.app' . Auth::user()->getRole())

@section('content')
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-3">
                @include('messages.menuNavegacion')
            </div>
            <div class="col-lg-9 animated fadeInRight">
                <div class="mail-box-header">
                    <div class="pull-right tooltip-demo">
                        <a href="{{ route('conversations.index') }}" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Descartar mensaje"><i class="fa fa-times"></i> Descartar</a>
                    </div>
                    <h2>
                        Crear mensaje
                    </h2>
                </div>
                <div class="mail-box">
                    <div class="mail-body">
                        <form id="mensaje" class="form-horizontal" method="POST" action="{{ route('mail.store') }}">
                        @csrf     
                            <div class="form-group {{ $errors->has('receptor') ? 'has-error' : '' }}">
                                <label class="col-sm-2 control-label">Para:</label>
                                <div class="col-sm-10">
                                    <select name="receptor" class="selectpicker form-control">
                                        <option value="" >Selecciona un destinatario</option>
                                        @foreach ($usuarios as $user)
                                            <option value="{{ $user->id }}" 
                                                data-content="<div style='width:100%;' class='row'>
                                                                    <div class='col-md-2'>
                                                                        <div class='client-avatar'>
                                                                            <img src='{{ asset($user->getFirstMediaUrl('avatar', 'thumb')) }}'>
                                                                        </div>
                                                                    </div>
                                                                    <div class='col-md-6'>
                                                                        <div><p class='text-primary text-capitalize font-italic font-normal'>{{ $user->name }}</p></div>
                                                                    </div>
                                                                    <div class='col-md-4 pull-right'>
                                                                        <div class='btn btn-{{ $user->getClassForRole() }} btn-xs' style='vertical-align:middle;'>{{ $user->getRoleNames()->first() }}</div>
                                                                    </div>
                                                                </div>">
                                            </option>
                                        @endforeach
                                    </select>
                                    <span class="invalid-feedback" role="alert">
                                        <strong class="text-danger">{{ $errors->first('receptor') }}</strong>
                                    </span>    
                                </div>
                            </div>
                            <br>
                            <div class="form-group">
                                <label class="col-sm-2 control-label"></label>
                                <div class="col-sm-10">
                                    <div class="i-checks">
                                        <label> 
                                            <input type="checkbox" name="broadcast" value="1"> <i> </i> Enviar a todos los {{ $user->destinatarios() }} 
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <br>
                            <div class="form-group {{ $errors->has('asunto') ? 'has-error' : '' }}">
                                <label class="col-sm-2 control-label">Asunto:</label>
                                <div class="col-sm-10">
                                    <input type="text" name="asunto" class="form-control" value="">
                                    <span class="invalid-feedback" role="alert">
                                        <strong class="text-danger">{{ $errors->first('asunto') }}</strong>
                                    </span>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="mail-text h-200">
                        <div class="form-group {{ $errors->has('mensaje') ? 'has-error' : '' }}">
                            <textarea form="mensaje" name="mensaje" class="summernote">

                            </textarea>
                        </div>
                        <span class="invalid-feedback" role="alert">
                            <strong class="text-danger">{{ $errors->first('mensaje') }}</strong>
                        </span>
                        <div class="clearfix"></div>
                    </div>
                    <div class="mail-body text-right tooltip-demo">
                        <button form="mensaje" type="submit" href="mailbox.html" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Enviar"><i class="fa fa-reply"></i> Enviar</button>
                        <a href="{{ route('conversations.index') }}" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Descartar mensaje"><i class="fa fa-times"></i> Descartar</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
@endsection
