@extends('layouts.app' . Auth::user()->getRole())

@section('content')
    <div class="wrapper wrapper-content">
        <div class="row">
            <div class="col-lg-3">
                @include('messages.menuNavegacion')
            </div>
            <div class="col-lg-9 animated fadeInRight">
                <div class="mail-box-header">
                    <div class="pull-right tooltip-demo">
                        <a href="{{ route('mail.index') }}" class="btn btn-danger btn-sm" data-toggle="tooltip" data-placement="top" title="Descartar mensaje"><i class="fa fa-times"></i> Descartar</a>
                    </div>
                    <h2>
                        Responder mensaje
                    </h2>
                </div>
                <div class="mail-box">
                    <div class="mail-body">
                        <form id="message" class="form-horizontal" method="POST" action="{{ route('conversations.update', $conversation) }}">
                        @csrf
                        @method('PUT')
                            <div class="form-group"><label class="col-sm-2 control-label">Para:</label>
                                <div class="col-sm-10">
                                    <input type="hidden" name="receptor" value="{{ $conversation->messages()->where('receptor_id', $usuario->id)->latest()->first()->emisor_id }}">
                                    <ul>
                                        <li  style="list-style:none;">
                                            <img class="img-thumbnail img-md" src="{{ asset($conversation->messages()->where('receptor_id', $usuario->id)->latest()->first()->emisor->getFirstMediaUrl('avatar', 'thumb')) }}" alt="">
                                            <span><strong>{{ $conversation->messages()->where('receptor_id', $usuario->id)->latest()->first()->emisor->name }}</strong></span>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label">Asunto:</label>
                                <div class="col-sm-10">
                                    <ul style="margin:auto;padding-top:7px;">
                                        <li style="list-style:none;font-style:oblique;">
                                            <div>{{ $conversation->asunto }}</div>
                                        </li>
                                    </ul>
                                </div>
                            </div>
                        </form>
                    </div>
                    <div class="mail-text h-200">
                        <br>
                        <p class="p-sm">Mensaje a responder:</p>

                        <div class="respuesta_mensaje p-sm" style=""> {!! $conversation->messages()->latest()->first()->message !!} </div>
                        
                        <textarea form="message" name="message" class="summernote">

                        </textarea>
                        <div class="clearfix"></div>
                    </div>
                    <div class="mail-body text-right tooltip-demo">
                        <button form="message" type="submit" href="mailbox.html" class="btn btn-sm btn-primary" data-toggle="tooltip" data-placement="top" title="Enviar"><i class="fa fa-reply"></i> Enviar</button>
                        <a href="{{ route('mail.index') }}" class="btn btn-white btn-sm" data-toggle="tooltip" data-placement="top" title="Descartar mensaje"><i class="fa fa-times"></i> Descartar</a>
                    </div>
                    <div class="clearfix"></div>
                </div>
            </div>
        </div>
    </div>
@endsection
