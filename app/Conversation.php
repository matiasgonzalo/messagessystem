<?php

namespace App;

use App\User;
use App\Message;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Model;

class Conversation extends Model
{
    protected $fillable = [
    'receptor_id',
    'emisor_id',
    'rol_latest_emisor', 
    'asunto',
    'id_latest_receptor',
    'name_latest_emisor',
    'latest_message', 
    'latest_message_date', 
    'read_by_emisor', 
    'read_by_receptor', 
    'deleted_logic_by_emisor', 
    'deleted_logic_by_receptor', 
    'deleted_fisic_by_emisor', 
    'deleted_fisic_by_receptor'];

    public function messages()
    {
        return $this->hasMany(Message::class, 'conversation_id');
    }

    public function emisor()
    {
        return $this->belongsTo(User::class, 'emisor_id');
    }

    public function receptor()
    {
        return $this->belongsTo(User::class, 'receptor_id');
    }

    public function tiempoTranscurrido(Carbon $date)
    {
        return $date->diffForHumans();
    }

    public function hora(Carbon $date)
    {
        return $date->format('h:i a');
    }

    public function fecha(Carbon $date)
    {
        return $date->format('d.m.Y');
    }

    public function latest_message_for_user(User $user)
    {
        return $this->messages()->where('receptor_id', $user->id)->latest()->first();
    }

    public function read_latest_message(User $user)
    {
        $message = $this->messages()->where('receptor_id', $user->id)->latest()->first();
        if ($message->read_by_receptor == true) {
            return 'read';
        }
        return 'unread';
    }
}
