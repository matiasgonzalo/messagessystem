<?php

namespace App\Http\Controllers;

use App\User;
use App\Message;
use Carbon\Carbon;
use App\Conversation;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Database\Eloquent\Builder;

class MessageController extends Controller
{
    protected $user;
    protected $emisor;
    protected $data;
    protected $message;

    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {
        $this->user = Auth::user();
        $conversations_a = Auth::user()->conversations_receptor()->where('deleted_first_level_receptor', false);

        $conversations_b = Auth::user()->conversations_emisor()->where('deleted_first_level_emisor', false)
                                                                ->whereHas('messages', function(Builder $query) {
                                                                    $query->where('receptor_id', '=', $this->user->id);
                                                                });

        $conversations = $conversations_a->union($conversations_b)->orderBy('latest_message_date', 'desc')->simplePaginate(10);
        $usuario = Auth::user();
        return view('messages.index', compact('conversations', 'usuario'));
    }

    public function indexEliminados()
    {
        $this->user = Auth::user();
        $conversations_a = Auth::user()->conversations_receptor()->where('deleted_first_level_receptor', true)
                                                                ->where('deleted_second_level_receptor', false);

        $conversations_b = Auth::user()->conversations_emisor()->where('deleted_first_level_emisor', true)
                                                                ->where('deleted_second_level_emisor', false)
                                                                ->whereHas('messages', function(Builder $query) {
                                                                    $query->where('receptor_id', '=', $this->user->id);
                                                                });

        $conversations = $conversations_a->union($conversations_b)->orderBy('latest_message_date', 'desc')->simplePaginate(10);

        $usuario = Auth::user();

        return view('messages.indexEliminados', compact('conversations', 'usuario'));
    }

    public function create()
    {
        if(Auth::user()->hasRole('vendedor'))  {
            
            $usuarios[0] = Auth::user()->partner;

            return view('messages.create', compact('usuarios'));

        }elseif(Auth::user()->hasRole('partner')) {

            $usuarios = User::where('partner_id', Auth::user()->id)
                            ->orWhere('id', Auth::user()->admin->id)
                            ->get();

            return view('messages.create', compact('usuarios'));

        }elseif(Auth::user()->hasRole('administrador')) {

            $usuarios1 = User::where('administrador_id', Auth::user()->id);
            $usuarios2 = User::role('superAdministrador');
            $usuarios = $usuarios1->union($usuarios2)->get();

            return view('messages.create', compact('usuarios'));

        }elseif(Auth::user()->hasRole('superAdministrador')) {
            
            $usuarios = User::where('id', '!=', Auth::user()->id)->get();
            return view('messages.create', compact('usuarios'));
            
        }else {
            return redirect()->route('conversations.index');
        }
    }

    public function store(Request $request)
    {    
        //Validacion
        $data = $request->validate([
            'asunto' => 'required',
            'mensaje' => 'required',
            'receptor' => ['nullable', 'integer', 'exists:users,id'],
            'broadcast' => ['nullable', 'boolean'],
        ]);

        $this->emisor = Auth::user();
        
        if(isset($data['broadcast'])){
            
            $role = $this->emisor->getRole();
            
            if($role == 'Administrador') {
                
                $receptores = $this->emisor->partners();       

            }elseif ($role == 'SuperAdministrador') {
                
                $receptores = User::where('id','!=' ,$this->emisor->id)->get();

            }elseif ($role == 'Partner') {
                
                $receptores = $this->emisor->vendedores();

            }
            
            if(isset($receptores)) {

                $this->data = $data;
                $receptores->each(function($receptor) {

                    $conversation = Conversation::create([
                        'id_latest_receptor' => $receptor->id,
                        'asunto' => $this->data['asunto'],
                        'latest_message' => $this->data['mensaje'],
                        'latest_message_date' => Carbon::now()
                    ]);
            
                    $message = Message::create([
                        'message' => $this->data['mensaje'],
                    ]);
            
                    $this->emisor->messages_emisor()->save($message);
            
                    $receptor->messages_receptor()->save($message);
            
                    $conversation->messages()->save($message);
            
                    $this->emisor->conversations_emisor()->save($conversation);
            
                    $receptor->conversations_receptor()->save($conversation);
                });
            }

        }elseif(!isset($data['receptor'])){
        //     //Store
            $receptor = User::find($data['receptor']);

            $conversation = Conversation::create([
                'id_latest_receptor' => $receptor->id,
                'asunto' => $data['asunto'],
                'latest_message' => $data['mensaje'],
                'latest_message_date' => Carbon::now()
            ]);
    
            $message = Message::create([
                'message' => $data['mensaje'],
            ]);
    
            $this->emisor->messages_emisor()->save($message);
    
            $receptor->messages_receptor()->save($message);
    
            $conversation->messages()->save($message);
    
            $this->emisor->conversations_emisor()->save($conversation);
    
            $receptor->conversations_receptor()->save($conversation);
        }

        // event(new ElMensajeFueEnviado($mensaje));

        $notification = [
            'alert-type' => 'success',
            'message' => __('El mensaje fue enviado con éxito.!!'),
        ];
        
        return redirect()->route('conversations.index')->with($notification);
    }

    public function update(Request $request, Conversation $conversation)
    {
        $data = $request->validate([
            'receptor' => ['required','integer' ,'exists:users,id'],
            'message' => ['required']
        ]);

        $receptor = User::find($data['receptor']);
        $emisor = Auth::user();

        $message = Message::create([
            'message' => $data['message']
        ]);

        $emisor->messages_emisor()->save($message);
        $receptor->messages_receptor()->save($message);

        $conversation->messages()->save($message);
        $conversation->id_latest_receptor = $receptor->id;
        $conversation->latest_message = $message->message;
        $conversation->latest_message_date = $message->created_at;
        $conversation->read_by_latest_receptor = false;

        if($receptor->id == $conversation->receptor_id) {
            $conversation->deleted_first_level_receptor = false;
            $conversation->deleted_second_level_receptor = false;
        }elseif ($receptor->id == $conversation->emisor_id) {
            $conversation->deleted_first_level_emisor = false;
            $conversation->deleted_second_level_emisor = false;
        }

        $conversation->save();

        $notification = [
            'alert-type' => 'success',
            'message' => __('El mensaje fue respondido con éxito.!!'),
        ];

        return redirect()->route('conversations.index')->with($notification);
    }

    public function store_mail_privado(Request $request)
    {
        //validation
        $this->validate($request, [
            'asunto' => 'required',
            'mensaje' => 'required',
        ]);

        //store
        if(auth()->user()->hasRole('vendedor')) 
        {
            $usuario = User::find(auth()->user()->partner->id);
            Mensaje::create([
                'emisor_id' => auth()->id(),
                'rol_emisor' => auth()->user()->getRole(),
                'receptor_id' => $usuario->id,
                'asunto' => $request->input('asunto'),
                'mensaje' => $request->input('mensaje')
            ]);
            
            event(new ElMensajeFueEnviado($mensaje));

            $notification = [
                'alert-type' => 'success',
                'message' => __('El mensaje fue enviado con éxito.!!'),
            ];

            return redirect()->route('conversations.index')->with($notification);

        }elseif(auth()->user()->hasRole('partner')) {
            $usuario = User::find(auth()->user()->admin->id);
            
            Mensaje::create([
                'emisor_id' => auth()->id(),
                'rol_emisor' => auth()->user()->getRole(),
                'receptor_id' => $usuario->id,
                'asunto' => $request->input('asunto'),
                'mensaje' => $request->input('mensaje')
            ]);
            
            event(new ElMensajeFueEnviado($mensaje));

            $notification = [
                'alert-type' => 'success',
                'message' => __('El mensaje fue enviado con éxito.!!'),
            ];
            
            return redirect()->route('conversations.index')->with($notification);

        }elseif(auth()->user()->hasRole('administrador')) {
            
            $usuario = User::role('superAdministrador')->first();

            Mensaje::create([
                'emisor_id' => auth()->id(),
                'rol_emisor' => auth()->user()->getRole(),
                'receptor_id' => $usuario->id,
                'asunto' => $request->input('asunto'),
                'mensaje' => $request->input('mensaje')
            ]);

            event(new ElMensajeFueEnviado($mensaje));

            $notification = [
                'alert-type' => 'success',
                'message' => __('El mensaje fue enviado con éxito.!!'),
            ];

            return redirect()->route('conversations.index')->with($notification);

        }else{

            return redirect()->route('conversations.index');
        }
    }

    public function show(Conversation $conversation)
    {
        $user = Auth::user();

        if (($user->id == $conversation->id_latest_receptor) && ($conversation->read_by_latest_receptor == false)) {
            $conversation->read_by_latest_receptor = true;
            $conversation->save();
            $messages = $conversation->messages()->where('receptor_id', $user->id)->where('read_by_receptor', false)->get();
            foreach ($messages as $key => $message) {
                $message->read_by_receptor = true;
                $message->save();
            }
        }

        $conversation->load(['messages' => function($query){
            $query->orderBy('created_at', 'desc');
        }]);

        return view('messages.show', compact('conversation'));
    }

    public function showEliminado(Conversation $conversation)
    {
        $user = Auth::user();

        $conversation->load(['messages' => function($query){
            $query->orderBy('created_at', 'desc');
        }]);

        return view('messages.showEliminado', compact('conversation'));
    }

    public function delete_first_level(Request $request)
    {
        $data = $request->validate([
            'conversation' => ['required', 'exists:conversations,id']
        ]);
        
        $usuario = Auth::user();
        $conversation = Conversation::find($data['conversation']);
        if($conversation->receptor_id == $usuario->id) {
            $conversation->deleted_first_level_receptor = true;
            $conversation->save();
        }

        if($conversation->emisor_id == $usuario->id) {
            $conversation->deleted_first_level_emisor = true;
            $conversation->save();
        }

        $notification = [
            'alert-type' => 'success',
            'message' => __('Los mensajes fueron eliminados con éxito.!!'),
        ];

        return redirect()->route('conversations.index')->with($notification);
    }

    public function delete_second_level(Request $request)
    {
        $data = $request->validate([
            'conversation' => ['required', 'exists:conversations,id']
        ]);
        
        $usuario = Auth::user();
        $conversation = Conversation::find($data['conversation']);
        if($conversation->receptor_id == $usuario->id) {
            $conversation->deleted_second_level_receptor = true;
            $conversation->save();
        }

        if($conversation->emisor_id == $usuario->id) {
            $conversation->deleted_second_level_emisor = true;
            $conversation->save();
        }

        if(($conversation->deleted_second_level_emisor == true) && ($conversation->deleted_second_level_receptor == true)) {
            $conversation->delete();
        }

        $notification = [
            'alert-type' => 'success',
            'message' => __('Los mensajes fueron eliminados con éxito.!!'),
        ];

        return redirect()->route('conversations.index_eliminados')->with($notification);
    }

    public function delete_first_level_group(Request $request)
    {
        $data = $request->validate([
            'conversations' => ['required', 'array'],
            'conversations.*' =>  ['integer','exists:conversations,id'],
        ]);

        $usuario = Auth::user();

        if($request->filled('conversations')) {
            $arreglo = $data['conversations'];
            $max = count($arreglo);
            for ($i=0; $i < $max; $i++) {
                $conversation = Conversation::find($arreglo[$i]);
                if($conversation->receptor_id == $usuario->id) {
                    $conversation->deleted_first_level_receptor = true;
                    $conversation->save();
                }
        
                if($conversation->emisor_id == $usuario->id) {
                    $conversation->deleted_first_level_emisor = true;
                    $conversation->save();
                }
            }

            $notification = [
                'alert-type' => 'success',
                'message' => __('Los mensajes fueron eliminados con éxito.!!'),
            ];

            return redirect()->route('conversations.index')->with($notification);
        }
    }

    public function delete_second_level_group(Request $request)
    {
        $this->user = Auth::user();
        $conversations_a = Auth::user()->conversations_receptor()->where('deleted_first_level_receptor', true)
                                                                ->where('deleted_second_level_receptor', false)
                                                                ->latest('latest_message_date');

        $conversations_b = Auth::user()->conversations_emisor()->where('deleted_first_level_emisor', true)
                                                                ->where('deleted_second_level_emisor', false)
                                                                ->whereHas('messages', function(Builder $query) {
                                                                    $query->where('receptor_id', '=', $this->user->id);
                                                                })->latest('latest_message_date');

        $conversations = $conversations_a->union($conversations_b)->get();
        
        $usuario = Auth::user();

        foreach($conversations as $conversation) {
            if($conversation->receptor_id == $usuario->id) {
                $conversation->deleted_second_level_receptor = true;
                $conversation->save();
            }

            if($conversation->emisor_id == $usuario->id) {
                $conversation->deleted_second_level_emisor = true;
                $conversation->save();
            }

            if(($conversation->deleted_second_level_emisor == true) && ($conversation->deleted_second_level_receptor == true)) {
                $conversation->delete();
            }
        }

        $notification = [
            'alert-type' => 'success',
            'message' => __('Los mensajes fueron eliminados con éxito.!!'),
        ];

        return redirect()->route('conversations.index_eliminados')->with($notification);
    }

    public function restore_conversations(Request $request)
    {
        $data = $request->validate([
            'conversations' => ['required', 'array'],
            'conversations.*' =>  ['integer','exists:conversations,id'],
        ]);

        $usuario = Auth::user();

        if($request->filled('conversations')) {
            $arreglo = $data['conversations'];
            $max = count($arreglo);
            for ($i=0; $i < $max; $i++) {
                $conversation = Conversation::find($arreglo[$i]);
                if($conversation->receptor_id == $usuario->id) {
                    $conversation->deleted_first_level_receptor = false;
                    $conversation->save();
                }
        
                if($conversation->emisor_id == $usuario->id) {
                    $conversation->deleted_first_level_emisor = false;
                    $conversation->save();
                }
            }
        }

        $notification = [
            'alert-type' => 'success',
            'message' => __('Los mensajes fueron restaurados con éxito.!!'),
        ];

        return redirect()->route('conversations.index_eliminados')->with($notification);
    }

    public function responder(Conversation $conversation)
    {
        $usuario = Auth::user();
        
        return view('messages.respuesta', compact('conversation', 'usuario'));
    }

    public function buscarMensajeRecibido(Request $request)
    {
        $data = $request->validate([
            'mensaje' => ['required'],
        ]);

        $this->message = $data['mensaje'];
        $this->user = Auth::user();
        $conversations_a = Auth::user()->conversations_receptor()->where('deleted_first_level_receptor', false)
                                                                    ->whereHas('messages', function(Builder $query){
                                                                        $query->where('message', 'like', '%' . $this->message . '%');
                                                                    });

        $conversations_b = Auth::user()->conversations_emisor()->where('deleted_first_level_emisor', false)
                                                                ->whereHas('messages', function(Builder $query) {
                                                                    $query->where('receptor_id', '=', $this->user->id)
                                                                            ->where('message', 'like', '%' . $this->message . '%');
                                                                });

        $conversations = $conversations_a->union($conversations_b)->orderBy('latest_message_date', 'desc')->simplePaginate(10);
        
        $usuario = Auth::user();

        return view('messages.index', compact('conversations', 'usuario'));
    }

    public function buscarMensajeEliminado(Request $request)
    {
        $data = $request->validate([
            'mensaje' => ['required'],
        ]);

        $this->message = $data['mensaje'];
        $this->user = Auth::user();
        $conversations_a = Auth::user()->conversations_receptor()->where('deleted_first_level_receptor', true)
                                                                ->where('deleted_second_level_receptor', false)
                                                                ->whereHas('messages', function(Builder $query){
                                                                    $query->where('message', 'like', '%' . $this->message . '%');
                                                                });

        $conversations_b = Auth::user()->conversations_emisor()->where('deleted_first_level_emisor', true)
                                                                ->where('deleted_second_level_emisor', false)
                                                                ->whereHas('messages', function(Builder $query) {
                                                                    $query->where('receptor_id', '=', $this->user->id)
                                                                            ->where('message', 'like', '%' . $this->message . '%');
                                                                });

        $conversations = $conversations_a->union($conversations_b)->orderBy('latest_message_date', 'desc')->simplePaginate(10);

        $usuario = Auth::user();

        return view('messages.indexEliminados', compact('conversations', 'usuario'));
    }
}
