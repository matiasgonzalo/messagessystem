<?php

namespace App\Http\Controllers\Admin;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdministradorController extends Controller
{
    public function index()
    {
        $usuarios = User::role('administrador')->get();
        return $usuarios;
    }

    public function show($id)
    {
        $usuario = User::find($id);
        return $usuario;
    }

    public function destroy($id)
    {
        $usuario = User::findOrFail($id);
        return $usuario;
    }
}
