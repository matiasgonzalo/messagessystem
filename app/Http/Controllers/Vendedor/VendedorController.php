<?php

namespace App\Http\Controllers\Vendedor;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class VendedorController extends Controller
{
    public function index()
    {
        $usuarios = User::role('vendedor')->get();
        return $usuarios;
    }

    public function show($id)
    {
        $usuario = User::findOrFail($id);
        return $usuario;
    }

    public function destroy($id)
    {
        $usuario = User::findOrFail($id);
        return $usuario;
    }
}
