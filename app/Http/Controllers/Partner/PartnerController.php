<?php

namespace App\Http\Controllers\Partner;

use App\User;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class PartnerController extends Controller
{
    public function index()
    {
        $usuarios = User::role('partner')->get();
        return $usuarios;
    }

    public function show($id)
    {
        $usuario = User::findOrFail($id);
        return $usuario;
    }

    public function destroy($id)
    {
        $usuario = User::findOrFail($id);
        return $usuario;
    }
}
