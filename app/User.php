<?php

namespace App;

use App\Message;
use App\Conversation;
use Illuminate\Support\Facades\Auth;
use Spatie\MediaLibrary\Models\Media;
use Spatie\Permission\Traits\HasRoles;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Builder;
use Spatie\MediaLibrary\HasMedia\HasMedia;
use Illuminate\Contracts\Auth\MustVerifyEmail;
use Spatie\MediaLibrary\HasMedia\HasMediaTrait;
use Illuminate\Foundation\Auth\User as Authenticatable;

class User extends Authenticatable implements HasMedia
{
    use Notifiable;
    use HasMediaTrait;
    use HasRoles;

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    /**
     * The attributes that should be cast to native types.
     *
     * @var array
     */
    protected $casts = [
        'email_verified_at' => 'datetime',
    ];

    //Un partner tiene muchos vendedores y un vendedor pertenece a un partner
    public function vendedores()
    {
        return $this->hasMany(User::class, 'partner_id');
    }

    public function partner()
    {
        return $this->belongsTo(User::class, 'partner_id');
    }

    //Un administrador tiene muchos partners y un partner pertenece a un administrador
    public function partners()
    {
        return $this->hasMany(User::class, 'administrador_id');
    }

    public function admin()
    {
        return $this->belongsTo(User::class, 'administrador_id');
    }

    public function conversations_emisor()
    {
        return $this->hasMany(Conversation::class, 'emisor_id');
    }

    public function conversations_receptor()
    {
        return $this->hasMany(Conversation::class, 'receptor_id');
    }

    public function messages_emisor()
    {
        return $this->hasMany(Message::class, 'emisor_id');
    }

    public function messages_receptor()
    {
        return $this->hasMany(Message::class, 'receptor_id');
    }

    public function messages_count_unread()
    {
        $messages_count_read = $this->messages_receptor()->where('read_by_receptor', false)->count();
        return $messages_count_read;
    }

    public function conversations_count()
    {
        $conversations_a = $this->conversations_receptor()->where('deleted_first_level_receptor', false)->where('deleted_second_level_receptor', false);
        $conversations_b = $this->conversations_emisor()->where('deleted_first_level_emisor', false)
                                                            ->where('deleted_second_level_emisor', false)
                                                                ->whereHas('messages', function(Builder $query){
                                                                    $query->where('receptor_id', '=', $this->id);       
                                                                });

        $conversations_count = $conversations_a->union($conversations_b)->count();

        return $conversations_count;
    }

    public function conversations_deleted_count()
    {
        $conversations_a = $this->conversations_receptor()->where('deleted_first_level_receptor', true)->where('deleted_second_level_receptor', false);
        $conversations_b = $this->conversations_emisor()->where('deleted_first_level_emisor', true)
                                                            ->where('deleted_second_level_emisor', false)
                                                                ->whereHas('messages', function(Builder $query){
                                                                    $query->where('receptor_id', '=', $this->id);       
                                                                });

        $conversations_count = $conversations_a->union($conversations_b)->count();

        return $conversations_count;
    }

    public function getRole()
    {
        return ucfirst($this->getRoleNames()->first());
    }

    public function getClassForRole()
    {
        $rol = $this->getRoleNames()->first();
        if ($rol == 'vendedor') {
            return 'primary';
        }elseif ($rol == 'administrador') {
            return 'warning';
        }elseif ($rol == 'partner') {
            return 'info';
        }elseif ($rol == 'superAdministrador'){
            return 'danger';
        }
        return '';
    }

    public function destinatarios()
    {
        if (Auth::user()->hasRole('superAdministrador')) {
            return 'Usuarios';
        }elseif (Auth::user()->hasRole('administrador')) {
            return 'Partners';
        }elseif (Auth::user()->hasRole('partner')) {
            return 'Vendedores';
        }else{
            return '';
        }
    }

    public function consultarRol(User $usuario)
    {
        if ($usuario->hasRole('vendedor')) {
            return 'Vendedor';
        }elseif ($usuario->hasRole('administrador')) {
            return 'Administrador';
        }elseif ($usuario->hasRole('partner')) {
            return 'Partner';
        }else{
            return 'Soporte Tecnico';
        }
    }

    public function registerMediaConversions(Media $media = null)
    {
        $this->addMediaConversion('thumb')
            ->width(100)
            ->height(100)
            ->performOnCollections('avatar');
    }

    public function registerMediaCollections()
    {
        $this->addMediaCollection('avatar')
            ->singleFile();
    }
}
